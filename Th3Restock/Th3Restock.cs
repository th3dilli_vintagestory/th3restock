using System.Collections.Generic;
using HarmonyLib;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace Th3Restock
{
    public class Th3Restock : ModSystem
    {
        private ICoreClientAPI _capi;

        private ICoreServerAPI _sapi;

        private Harmony harmony;

        private readonly string harmonyPatchkey = "Th3Restock.Patch";

        public override void StartClientSide(ICoreClientAPI capi)
        {
            _capi = capi;
            _capi.Input.RegisterHotKey("th3restock", Lang.Get("th3restock:restock-cmd"), GlKeys.R);
            _capi.Input.SetHotKeyHandler("th3restock", OnRestock);
        }

        public override void StartServerSide(ICoreServerAPI sapi)
        {
            _sapi = sapi;
            _sapi.ChatCommands.Create("restock")
                .WithDescription(Lang.Get("th3restock:restock-cmd"))
                .RequiresPrivilege(Privilege.chat)
                .HandleWith(OnRestockCommand)
                .Validate();

            _sapi.ChatCommands.Create("autorestock")
                .WithDescription(Lang.Get("th3restock:autorestock-cmd"))
                .RequiresPrivilege(Privilege.chat)
                .WithArgs(_sapi.ChatCommands.Parsers.Bool("enable"))
                .HandleWith(OnAutorestock)
                .Validate();

            _sapi.Event.DidPlaceBlock += OnDidPlaceBlock;

            harmony = new Harmony(harmonyPatchkey);
            harmony.PatchAll();
        }

        private TextCommandResult OnAutorestock(TextCommandCallingArgs args)
        {
            var enable = (bool)args.Parsers[0].GetValue();

            var player = ((IServerPlayer)args.Caller.Player);

            if (enable)
            {
                player.ServerData.CustomPlayerData.Remove("autorestock");
                return TextCommandResult.Success(Lang.Get("th3restock:autorestock-cmd-enabled"));
            }

            player.ServerData.CustomPlayerData.TryGetValue("autorestock", out var value);
            if (value == null)
            {
                player.ServerData.CustomPlayerData.Add("autorestock", "false");
            }
            else
            {
                player.ServerData.CustomPlayerData["autorestock"] = "false";
            }

            return TextCommandResult.Success(Lang.Get("th3restock:autorestock-cmd-disabled"));
        }

        private void OnDidPlaceBlock(IServerPlayer byPlayer, int oldblockId, BlockSelection blockSel,
            ItemStack withItemStack)
        {
            if (withItemStack?.StackSize == 1)
            {
                OnRestockCommand(byPlayer);
            }
        }

        private bool OnRestock(KeyCombination t1)
        {
            ItemStack activeItemStack = _capi.World.Player.InventoryManager.ActiveHotbarSlot.Itemstack;
            if (activeItemStack != null)
            {
                int MaxStackSize = 0;
                if (activeItemStack.Block != null)
                {
                    MaxStackSize = activeItemStack.Block.MaxStackSize;
                }
                else if (activeItemStack.Item != null)
                {
                    MaxStackSize = activeItemStack.Item.MaxStackSize;
                }

                if (activeItemStack.StackSize < MaxStackSize && CanRestock(_capi.World.Player))
                {
                    _capi.SendChatMessage("/restock");
                }
            }

            return true;
        }

        private TextCommandResult OnRestockCommand(TextCommandCallingArgs args)
        {
            var player = ((IServerPlayer)args.Caller.Player);

            OnRestockCommand(player);
            return TextCommandResult.Success();
        }

        private void OnRestockCommand(IServerPlayer player)
        {
            player.ServerData.CustomPlayerData.TryGetValue("autorestock", out string value);
            if (value != "false")
            {
                Restock(_sapi, player);
            }
        }
        
        
        public static void OnRestockCommand(ICoreServerAPI sapi, IServerPlayer player)
        {
            player.ServerData.CustomPlayerData.TryGetValue("autorestock", out string value);
            if (value != "false")
            {
                Restock(sapi, player);
            }
        }

        public static void Restock(ICoreServerAPI api, IPlayer player)
        {
            ItemSlot activeSlot = player.InventoryManager.ActiveHotbarSlot;
            ItemStack activeItemStack = activeSlot.Itemstack;
            if (activeItemStack != null)
            {
                int ItemId = activeItemStack.Id;
                int MaxStackSize = 0;
                if (activeItemStack.Block != null)
                {
                    MaxStackSize = activeItemStack.Block.MaxStackSize;
                }
                else if (activeItemStack.Item != null)
                {
                    MaxStackSize = activeItemStack.Item.MaxStackSize;
                }

                if (activeItemStack.StackSize < MaxStackSize)
                {
                    List<IInventory> inventories = new List<IInventory>
                    {
                        player.InventoryManager.GetHotbarInventory(),
                        player.InventoryManager.GetOwnInventory("backpack")
                    };
                    foreach (IInventory inv in inventories)
                    {
                        if (inv is ItemSlotCreative)
                        {
                            continue;
                        }

                        foreach (ItemSlot itemSlot in inv)
                        {
                            if (itemSlot.Itemstack != null)
                            {
                                if (itemSlot.Itemstack.Id == ItemId)
                                {
                                    ItemStackMoveOperation op = new ItemStackMoveOperation(api.World,
                                        EnumMouseButton.Left, EnumModifierKey.SHIFT, EnumMergePriority.DirectMerge,
                                        itemSlot.StackSize);
                                    itemSlot.TryPutInto(activeSlot, ref op);
                                    itemSlot.MarkDirty();
                                    if (activeItemStack.StackSize == MaxStackSize)
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public override void Dispose()
        {
            if (harmony != null)
            {
                harmony.UnpatchAll(harmonyPatchkey);
            }
        }

        public static bool CanRestock(IPlayer player)
        {
            if (player.InventoryManager.ActiveHotbarSlot.Itemstack != null)
            {
                int ItemId = player.InventoryManager.ActiveHotbarSlot.Itemstack.Id;
                List<IInventory> inventories = new List<IInventory>
                {
                    player.InventoryManager.GetHotbarInventory(),
                    player.InventoryManager.GetOwnInventory("backpack")
                };
                int found = 0;
                foreach (IInventory inv in inventories)
                {
                    if (inv is ItemSlotCreative)
                    {
                        continue;
                    }

                    foreach (ItemSlot itemSlot in inv)
                    {
                        if (itemSlot.Itemstack != null)
                        {
                            if (itemSlot.Itemstack.Id == ItemId)
                            {
                                found++;
                                if (found > 1)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }
    }

    [HarmonyPatch(typeof(BlockEntityGroundStorage), "TryPutItem")]
    public class PatchBlockEntityGroundStorage
    {
        public static void Postfix(BlockEntityGroundStorage __instance, IPlayer player)
        {
            // __instance.Api.Logger.VerboseDebug($"put oss={__instance.Inventory}");

            ItemStack activeItemStack = player.InventoryManager.ActiveHotbarSlot.Itemstack;
            if (activeItemStack != null)
            {
                int MaxStackSize = 0;
                int restockAmount = player.Entity.Controls.CtrlKey
                    ? __instance.BulkTransferQuantity
                    : __instance.TransferQuantity;

                if (activeItemStack.Block != null)
                {
                    MaxStackSize = activeItemStack.Block.MaxStackSize;
                }
                else if (activeItemStack.Item != null)
                {
                    MaxStackSize = activeItemStack.Item.MaxStackSize;
                }

                // __instance.Api.Logger.VerboseDebug(
                //     $"as={activeItemStack.StackSize} r={restockAmount} m={MaxStackSize} sp={player.Entity.Controls.Sprint} sn={player.Entity.Controls.Sneak} can={activeItemStack.StackSize < MaxStackSize && activeItemStack.StackSize <= restockAmount && Th3Restock.CanRestock(player)}");

                if (activeItemStack.StackSize < MaxStackSize && activeItemStack.StackSize <= restockAmount &&
                    Th3Restock.CanRestock(player))
                {
                    if (__instance.Api is ICoreServerAPI sapi)
                    {
                        Th3Restock.OnRestockCommand(sapi, (IServerPlayer)player);
                    }
                    // else
                    // {
                        // __instance.Api.Logger.VerboseDebug("not server TryPutItem");
                    // }
                }
            }
        }
    }
    [HarmonyPatch(typeof(BlockEntityItemPile), "TryPutItem")]
    public class PatchBlockEntityItemPile
    {
        public static void Postfix(BlockEntityItemPile __instance, IPlayer player)
        {
            // __instance.Api.Logger.VerboseDebug($"put oss={__instance.OwnStackSize}");

            ItemStack activeItemStack = player.InventoryManager.ActiveHotbarSlot.Itemstack;
            if (activeItemStack != null)
            {
                int MaxStackSize = 0;
                int restockAmount = player.Entity.Controls.Sprint ? __instance.BulkTakeQuantity : __instance.DefaultTakeQuantity;

                if (activeItemStack.Block != null)
                {
                    MaxStackSize = activeItemStack.Block.MaxStackSize;
                }
                else if (activeItemStack.Item != null)
                {
                    MaxStackSize = activeItemStack.Item.MaxStackSize;
                }

                // __instance.Api.Logger.VerboseDebug($"as={activeItemStack.StackSize} r={restockAmount} m={MaxStackSize} sp={player.Entity.Controls.Sprint} sn={player.Entity.Controls.Sneak} can={activeItemStack.StackSize < MaxStackSize && activeItemStack.StackSize <= restockAmount && Th3Restock.CanRestock(player)}");

                if (activeItemStack.StackSize < MaxStackSize && activeItemStack.StackSize <= restockAmount && Th3Restock.CanRestock(player))
                {
                    if (__instance.Api is ICoreServerAPI sapi)
                    {
                        Th3Restock.OnRestockCommand(sapi, (IServerPlayer)player);
                    }
                    // else
                    // {
                        // __instance.Api.Logger.VerboseDebug("not server TryPutItem");
                    // }
                }
            }
        }
    }
}