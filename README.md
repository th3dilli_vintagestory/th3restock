# Th3Restock

This mod adds the ability to restock your current hotbar slot. You can either do it manually by pressing the Hotkey (Default: R, can be changend in the Setting > Controls) or automatically for blocks.

When building and the current stack reaches one block it will refill from hotbar and backpack. This automatic restock can be disabled by `/autorestock false`
![](preview/build_restock.gif)

When pressing the Hotkey (Default: R) blocks/items will restock into current active hotbar slot
![](preview/press_restock.gif)
